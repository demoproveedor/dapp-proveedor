﻿using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.Web3;
using System;
using System.Numerics;
using System.Threading.Tasks;
using System.Web.Http;
using WebServices.Models;

namespace WebServices.Controllers
{
    public class ProveedorController : ApiController
    {
        [HttpPost]
        public Response addProvedor(Proveedor proveedor)
        {

            string url = "HTTP://localhost:7545";
            string address = "0xf5578DF4DA40089e80d0655aea109907D4ea4B2a";
            string ABI = @"[{'constant':true,'inputs':[{'name':'_RFC','type':'string'}],'name':'RequestProveedor','outputs':[{'name':'','type':'bool'}],'payable':false,'stateMutability':'view','type':'function'},{'constant':false,'inputs':[{'name':'_RFC','type':'string'},{'name':'_razonSocial','type':'string'},{'name':'_domicilio','type':'string'}],'name':'AddProveedor','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'}]";
            Web3 web3 = new Web3(url);
            Contract ProveedorContract = web3.Eth.GetContract(ABI, address);
            string Response_ = null;
            if (proveedor.AccountAddress != string.Empty || proveedor.RFC != string.Empty || proveedor.RazonSocial != string.Empty || proveedor.Domicilio != string.Empty)
            {
                try
                {
                    HexBigInteger gas = new HexBigInteger(new BigInteger(4000000));
                    HexBigInteger value = new HexBigInteger(new BigInteger(0));
                    Task<string> addProveedorFunction = ProveedorContract.GetFunction("AddProveedor").SendTransactionAsync(proveedor.AccountAddress, gas, value, proveedor.RFC, proveedor.RazonSocial, proveedor.Domicilio);
                    addProveedorFunction.Wait();
                    Response_ = addProveedorFunction.Result.ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw ex;
                }
            }
            var resp = new Response { IsSuccess = true, Mensaje = Response_ };
            return resp;
        }
        [HttpPost]
        public Response getProvedorExist(Proveedor proveedor)
        {
            try
            {
                bool existe = false;

                string url1 = "HTTP://localhost:7545";
                string address1 = "0xf5578DF4DA40089e80d0655aea109907D4ea4B2a";
                string ABI1 = @"[{'constant':true,'inputs':[{'name':'_RFC','type':'string'}],'name':'RequestProveedor','outputs':[{'name':'','type':'bool'}],'payable':false,'stateMutability':'view','type':'function'},{'constant':false,'inputs':[{'name':'_RFC','type':'string'},{'name':'_razonSocial','type':'string'},{'name':'_domicilio','type':'string'}],'name':'AddProveedor','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'}]";
                Web3 web31 = new Web3(url1);
                Contract ProveedorContract1 = web31.Eth.GetContract(ABI1, address1);

                if (proveedor.RFC != null)
                {
                    try
                    {
                        HexBigInteger gas = new HexBigInteger(new BigInteger(4000000));
                        HexBigInteger value = new HexBigInteger(new BigInteger(0));
                        Task<Boolean> getRFCFunction = ProveedorContract1.GetFunction("RequestProveedor").CallAsync<Boolean>(proveedor.RFC);
                        getRFCFunction.Wait();
                        existe = getRFCFunction.Result;

                        if (existe)
                        {
                            return new Response { IsSuccess = true, Mensaje = "Existe" };
                        }
                        return new Response { IsSuccess = false, Mensaje = "No existe" };
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error:", ex.Message);
                        throw ex;
                    }

                }
                return new Response { IsSuccess = false, Mensaje = "No hizo nada" };

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }
        }
       
    }
}

