﻿using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
//using AppBC.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Nethereum.Contracts;
using Nethereum.Hex.HexTypes;
using Nethereum.JsonRpc.Client;
using Nethereum.Quorum;
using Nethereum.RPC.Eth.DTOs;
using Nethereum.RPC.TransactionReceipts;
using Nethereum.Web3;
using WebServices.Models;

namespace ApiWeb3.Controllers
{
    [Route("api/Proveedor/")]
    [ApiController]
    public class ProveedorController : ControllerBase 
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()

        {
            return new string[] { "ESUCHANDO A CONTROLADOR:", "Proveedor" };
        }
        [Route("addProvedor")]
        [HttpPost]
        public Response addProvedor(Proveedor proveedor)
        {
            string url = "HTTP://localhost:7545";
            string address = "0xf5578DF4DA40089e80d0655aea109907D4ea4B2a";
            string ABI = @"[{'constant':true,'inputs':[{'name':'_RFC','type':'string'}],'name':'RequestProveedor','outputs':[{'name':'','type':'bool'}],'payable':false,'stateMutability':'view','type':'function'},{'constant':false,'inputs':[{'name':'_RFC','type':'string'},{'name':'_razonSocial','type':'string'},{'name':'_domicilio','type':'string'}],'name':'AddProveedor','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'}]";
            Web3 web3 = new Web3(url);
            Contract ProveedorContract = web3.Eth.GetContract(ABI, address);
            string Response_ = null;
            if (proveedor.AccountAddress != string.Empty || proveedor.RFC != string.Empty || proveedor.RazonSocial != string.Empty || proveedor.Domicilio != string.Empty)
            {
                try
                {
                    HexBigInteger gas = new HexBigInteger(new BigInteger(4000000));
                    HexBigInteger value = new HexBigInteger(new BigInteger(0));
                    Task<string> addProveedorFunction = ProveedorContract.GetFunction("AddProveedor").SendTransactionAsync(proveedor.AccountAddress, gas, value, proveedor.RFC, proveedor.RazonSocial, proveedor.Domicilio);
                    addProveedorFunction.Wait();
                    Response_ = addProveedorFunction.Result.ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw ex;
                }
            }
            var resp = new Response { IsSuccess = true, Mensaje = Response_ };
            return resp;
        }
        //[Route("getProvedorExist")]
        //[HttpPost]
        //public Response getProvedorExist(Proveedor proveedor)
        //{
        //    try
        //    {
        //        bool existe = false;

        //        string url1 = "HTTP://localhost:7545";
        //        string address1 = "0xf5578DF4DA40089e80d0655aea109907D4ea4B2a";
        //        string ABI1 = @"[{'constant':true,'inputs':[{'name':'_RFC','type':'string'}],'name':'RequestProveedor','outputs':[{'name':'','type':'bool'}],'payable':false,'stateMutability':'view','type':'function'},{'constant':false,'inputs':[{'name':'_RFC','type':'string'},{'name':'_razonSocial','type':'string'},{'name':'_domicilio','type':'string'}],'name':'AddProveedor','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'}]";
        //        Web3 web31 = new Web3(url1);
        //        Contract ProveedorContract1 = web31.Eth.GetContract(ABI1, address1);

        //        if (proveedor.RFC != null)
        //        {
        //            try
        //            {
        //                //HexBigInteger gas = new HexBigInteger(new BigInteger(4000000));
        //                //HexBigInteger value = new HexBigInteger(new BigInteger(0));
        //                Task<Boolean> getRFCFunction = ProveedorContract1.GetFunction("RequestProveedor").CallAsync<Boolean>(proveedor.RFC);
        //                getRFCFunction.Wait();
        //                existe = getRFCFunction.Result;

        //                if (existe)
        //                {
        //                    return new Response { IsSuccess = true, Mensaje = "Existe" };
        //                }
        //                return new Response { IsSuccess = false, Mensaje = "No existe" };
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.WriteLine("Error:", ex.Message);
        //                throw ex;
        //            }

        //        }
        //        return new Response { IsSuccess = false, Mensaje = "No hizo nada" };

        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex.Message);
        //        throw ex;

        //    }
        //}

        //[Route("getCreateWallet")]
        //[HttpPost]
        //public Response craeteWallet(Proveedor proveedor)
        //{

        //    try
        //    {
        //        var web3 = new Web3("https://mainnet.infura.io/v3/7238211010344719ad14a89db874158c");
        //        var balance = web3.Eth.GetBalance.SendRequestAsync("0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae");
        //        var etherAmount = Web3.Convert.FromWei(balance.Result);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    var resp = new Response { IsSuccess = true };
        //    return resp;
        //            }
        [Route("QuorumPost")]
        [HttpPost]
        public void QuorumPost()
        {
            try
            {
                Web3 web3 = new Web3("https://192.168.56.101:22000");
               
             
                string addresContract = "0x7af7c387f3081b364eeab991941e10aa34cf0d67";
                string abi = @"[{'constant':false,'inputs':[{'name':'x','type':'string'}],'name':'set','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'},{'constant':true,'inputs':[],'name':'get','outputs':[{'name':'retVal','type':'string'}],'payable':false,'stateMutability':'view','type':'function'}]";
                string addresNodeAccount = "0x31ad7a4f6caeb23ef5e07820f73d0f707f5b3bb6";
                string mensaje = "Sergiolopez";
                Contract Contract = web3.Eth.GetContract(abi, addresContract);
       
                HexBigInteger gas = new HexBigInteger(new BigInteger(4000000));
                HexBigInteger value = new HexBigInteger(new BigInteger(0));

                Task<string> TransactionFunction = Contract.GetFunction("set").SendTransactionAsync(addresNodeAccount, gas, value, mensaje);
                TransactionFunction.Wait();
                var transaccionHash = TransactionFunction.Result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [Route("QuorumGet")]
        [HttpPost]
        public void QuorumGet()
        {
            try
            {
                Web3 web3 = new Web3("https://192.168.56.101:22000");               
                string addresContract = "0x7af7c387f3081b364eeab991941e10aa34cf0d67";
                string abi = @"[{'constant':false,'inputs':[{'name':'x','type':'string'}],'name':'set','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'},{'constant':true,'inputs':[],'name':'get','outputs':[{'name':'retVal','type':'string'}],'payable':false,'stateMutability':'view','type':'function'}]";
                
                Contract Contract = web3.Eth.GetContract(abi, addresContract);
                Task<String> getRFCFunction = Contract.GetFunction("get").CallAsync<String>();
                getRFCFunction.Wait();
                var callHecha = getRFCFunction.Result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [Route("QuorumPostPrivate")]
        [HttpPost]
        public async void QuorumPostPrivate()
        {
            try
            {
                string ip = "http://192.168.56.101";
                string port = "22000";
                string port2 = "30300";
                string url = ip +":"+ port;
                string auth = "123456";
               
                string KeyNode0 = "e7f3f94b5923aa6c354afff39d8b34c22ebd1f78d894da3627a92731b25e3a3e";
                //var urlIPCNode0 = "/home/quorum/quorum/quorum/istanbul/istanbul-tools/node0/data/geth.ipc";
                
                string KeyNode1 = "5fc117235cdcb5cc81d79aa7e02b8c8011c61c2f34790c5391e67f59041e0e3a";
                string addresNodeAccount = "0x31ad7a4f6caeb23ef5e07820f73d0f707f5b3bb6";
                
                string addresContract = "0x7af7c387f3081b364eeab991941e10aa34cf0d67";
                string abi = "[{'constant':false,'inputs':[{'name':'x','type':'string'}],'name':'set','outputs':[],'payable':false,'stateMutability':'nonpayable','type':'function'},{'constant':true,'inputs':[],'name':'get','outputs':[{'name':'retVal','type':'string'}],'payable':false,'stateMutability':'view','type':'function'}]";
                string bytecode = "0x608060405234801561001057600080fd5b50610336806100206000396000f3fe60806040526004361061004c576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680634ed3885e146100515780636d4ce63c14610119575b600080fd5b34801561005d57600080fd5b506101176004803603602081101561007457600080fd5b810190808035906020019064010000000081111561009157600080fd5b8201836020820111156100a357600080fd5b803590602001918460018302840111640100000000831117156100c557600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506101a9565b005b34801561012557600080fd5b5061012e6101c3565b6040518080602001828103825283818151815260200191508051906020019080838360005b8381101561016e578082015181840152602081019050610153565b50505050905090810190601f16801561019b5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b80600090805190602001906101bf929190610265565b5050565b606060008054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561025b5780601f106102305761010080835404028352916020019161025b565b820191906000526020600020905b81548152906001019060200180831161023e57829003601f168201915b5050505050905090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106102a657805160ff19168380011785556102d4565b828001600101855582156102d4579182015b828111156102d35782518255916020019190600101906102b8565b5b5090506102e191906102e5565b5090565b61030791905b808211156103035760008160009055506001016102eb565b5090565b9056fea165627a7a72305820f20f0c67565695e98eb5ccee101972404d210b6a51ef460bb1a76adefdafca190029";
                string mensajeSend = "Sergiolopez";


                //var byteArray = Encoding.ASCII.GetBytes(USER + ":" + PASS);
                //AuthenticationHeaderValue auth = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                //IClient client = new RpcClient(new Uri(url), auth, null, null, null);
                //var web3 = new Web3Quorum(client);



                //Web3Quorum web3Quorum = new Web3Quorum(url);
                Web3Quorum web3Quorum = new Web3Quorum(new QuorumAccount(addresNodeAccount), url);
                TransactionReceiptPollingService transactionService = new TransactionReceiptPollingService(web3Quorum.TransactionManager);
                web3Quorum.TransactionManager.DefaultGas = 0;
                //var account = await web3Quorum.Eth.CoinBase.SendRequestAsync();//"0x10d7bc21a0b6324fdc7c71f7ffd82a36423c4a62"
                Contract contrato = web3Quorum.Eth.GetContract(abi, addresContract);
                Function ContractFunction = contrato.GetFunction("set");
                bool existeFuncion = await ContractFunction.CallAsync<bool>();
                

                bool isUnlocked = await web3Quorum.Personal.UnlockAccount.SendRequestAsync(addresNodeAccount, "123456", 60);

                List<string> privateFor = new List<string>(new[] { KeyNode0 + "=" });//Revisar si es la key del nodo o la account
                web3Quorum.SetPrivateRequestParameters(privateFor);

                string txnHash = await ContractFunction.SendTransactionAsync(addresNodeAccount, mensajeSend);
                var receipt = await web3Quorum.Eth.Transactions.GetTransactionReceipt.SendRequestAsync(txnHash);
                
                TransactionReceipt transaccion = await transactionService.SendRequestAndWaitForReceiptAsync(() => ContractFunction.SendTransactionAsync(addresNodeAccount, mensajeSend));
                var txthash = transaccion.TransactionHash.ToString();
                
                //var transactionHash = await web3Quorum.Eth.DeployContract.SendRequestAsync(abi, byteCode, newAccount.Result, new HexBigInteger(50000000), MORTA_CONTRACT, name);
                //var transactionService = new QuorumTransactionManager(null, addresContract);
                //HexBigInteger gas = new HexBigInteger(new BigInteger(1000000));
                //HexBigInteger value = new HexBigInteger(new BigInteger(0));
                //Message = "private transaction manager not in use"
                //var txnHash =  ContractFunction.SendTransactionAsync(addresContract, mensajeSend);
                //var txnHash2 = ContractFunction.SendTransactionAndWaitForReceiptAsync(addresContract, null, mensajeSend);
                //txnHash2.Wait();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

//revisar este ejemplo para eso uso de conecciones de web3Quorum para cada funcion de la api.
//public static Web3Quorum EstablishPrivateBlockchainConnection(String rpcEndpoint)
//{
//    var byteArray = Encoding.ASCII.GetBytes(USER + ":" + PASS);
//    AuthenticationHeaderValue auth = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
//    IClient client = new RpcClient(new Uri(rpcEndpoint), auth, null, null, null);
//    var web3 = new Web3Quorum(client);
//    return web3;
//}

