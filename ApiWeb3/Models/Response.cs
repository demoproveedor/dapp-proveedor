﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServices.Models
{
    public class Response
    {
        public bool IsSuccess { get; set; }
        public string Mensaje { get; set; }
        public string token { get; set; }
    }
}