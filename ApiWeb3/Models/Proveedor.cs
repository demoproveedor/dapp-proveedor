﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebServices.Models
{
    public class Proveedor:Response
    {
        public string AccountAddress { get; set; }
        public string RFC { get; set; }
        public string RazonSocial { get; set; }
        public string Domicilio { get; set; }
        public bool Existe { get; set; }
    }
}