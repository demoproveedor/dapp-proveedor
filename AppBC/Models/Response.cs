﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppBC.Models
{
    public class Response
    {
        public bool IsSuccess { get; set; }
        public string Mensaje { get; set; }
        public string token { get; set; }
    }
}
