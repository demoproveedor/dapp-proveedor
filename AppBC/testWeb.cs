﻿using System;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;

namespace AppBC
{
    public class testWeb
    {
        public string adrsAccount_;
        public string domicilio_;
        public string rfc_;
        public string razonSocial_;
        public bool existe_;
        public string hash_;

        public string hash1
        {
            get { return this.hash_; }
            set { this.hash_ = value; }
        }

        public bool existe
        {
            get { return this.existe_; }
            set { this.existe_ = value; }
        }

        public string addressAccount
        {
            get { return this.adrsAccount_; }
            set { this.adrsAccount_ = value; }
        }
        public string domicilio
        {
            get { return this.domicilio_; }
            set { this.domicilio_ = value; }
        }
        public string rfc
        {
            get { return this.rfc_; }
            set { this.rfc_ = value; }
        }
        public string razonSocial
        {
            get { return this.razonSocial_; }
            set { this.razonSocial_ = value; }
        }


        ApiRest apiRest = new ApiRest();

        public ICommand buttonAdd
        {
            get
            {
                return new RelayCommand(addProveedor);
            }
        }
        public async void addProveedor()
        {
            try
            {
                
                var hash = await apiRest.AddProveedor(addressAccount, rfc, razonSocial, domicilio);
                var hash1 = hash.Mensaje.ToString();
                await Application.Current.MainPage.DisplayAlert(
                                                 "Correcto",
                                                 "Se agregó un nuevo proveedor \n" + hash1 ,
                                                 "Aceptar");
                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ICommand buttonRequest
        {
            get { return new RelayCommand(requestProv); }
        }

        public async void requestProv()
        {
            try
            {
                await Application.Current.MainPage.Navigation.PushAsync(new Consultar());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ICommand Consultar
        {
            get
            {
                return new RelayCommand(requestProveedor);
            }
        }
        public async void requestProveedor()
        {
            try
            {
                var x = await apiRest.getProvedorExist(rfc);

                if (x.Mensaje == "Existe")
                {
                    await Application.Current.MainPage.DisplayAlert(
                                                 "Aviso",
                                                 "Ya se encuentra registrado el RFC en la Agroblockchain",
                                                 "Aceptar");
                    return;
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert(
                                                  "Aviso",
                                                  "NO se encuentra registrado el RFC en la Agroblockchain",
                                                  "Aceptar");
                    return;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    } 
}
