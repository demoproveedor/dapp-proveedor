﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xamarin.Forms;
using AppBC.Models;

namespace AppBC
{
    public class ApiRest
    {
        private string xURL = Application.Current.Resources["UrlAPI"].ToString();

        public async Task<Response> AddProveedor(string accountAddress, string rfc, string razonSocial, string domicilio)
        {
            try
            {
                xURL += "api/Proveedor/addProvedor";
                HttpClient client = new HttpClient();
                var modelo = new { AccountAddress = accountAddress, RFC = rfc, RazonSocial = razonSocial, Domicilio = domicilio};
                var json = JsonConvert.SerializeObject(modelo);
                HttpContent content = new StringContent(json);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PostAsync(xURL, content);
                var resultJSON = await response.Content.ReadAsStringAsync();
                var taskModel = JsonConvert.DeserializeObject<Response>(resultJSON);
                return taskModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Response> getProvedorExist(string rfc)
        {
            try
            {
                xURL += "api/Proveedor/getProvedorExist";
                HttpClient client = new HttpClient();
                var modelo = new { RFC = rfc};
                var json = JsonConvert.SerializeObject(modelo);
                HttpContent content = new StringContent(json);
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var response = await client.PostAsync(xURL, content);
                var resultJSON = await response.Content.ReadAsStringAsync();
                var existe = JsonConvert.DeserializeObject<Response>(resultJSON);
                return existe;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
